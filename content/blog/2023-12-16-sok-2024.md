---
date: 2023-12-16
title: Beginning of Season of KDE 2024!
categories: [SoK]
author: Johnny Jazeix
summary: SoK 2024 is back...
SPDX-License-Identifier: CC-BY-SA-4.0
authors:
- SPDX-FileCopyrightText: 2023 Johnny Jazeix <jazeix@gmail.com>
---

## Beginning of SoK 2024!
New year, new Season!

This year, we are trying out a new way to handle the program. No more season.kde.org, everything is on invent.kde.org.

# For mentors
Send a request to kde-soc-management@kde.org to be added to the group of mentors watch the project so you get notified when contributors create their proposals.

We will create a side issue which lets you select the projects you want to mentor and the ones you reject.

Note that you should not write in the comments whether the projects have been accepted or rejected until the admins publish the results.

# For proposals
Create a new issue in our [repository](https://invent.kde.org/teams/season-of-kde/2024/-/issues) using the template **sok_proposal**. Add your proposal and tell the potential mentors about your idea.
The issue will be *confidential*, meaning only you and the mentors will have access.
The template will automatically add the labels for the program. You must not modify them.

If your project is accepted, it will become public, so don't enter any confidential personal information here.

If it already exists, select a label corresponding to your project. It will make filtering easier. If it the label does not exist, ping someone in the kde-soc channel, and an admin will add it when they can.

# Selection
The issues will be labelled as **accepted** or **rejected**. Accepted projects will become visible.

# Progress
Then, until the end of event, you will work on your chosen task, helped along by your mentors.
At the end of the task, the projects will be labelled **completed** or **not completed** depending on their status.

KDE expects you to write and publish at least two blog posts. If you don't have a blog, create a pull request in this repository to create a blog post and we will post it on this website. One post needs to be written during the project to explain your progress.

Then, KDE encourages yout to post a shared blog post along with all the other participants at the end of SoK to sum up what has been done. This post will be published on this website.

The admins will then contact the participants that completed their projects to get information on where to send your KDE goodies.
