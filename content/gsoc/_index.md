---
title: GSoC
layout: GSoC
menu:
#   main:
#     name: GSoC
#     weight: 2
---

Google Summer of Code is a program sponsored by Google to help developers start in open source software.
You can learn more about the Google Summer of Code at https://summerofcode.withgoogle.com/.

KDE has been participating to the GSoC since the beginning, in 2005, mentoring multiple projects every years and doing our best to include the new generation of developers. One of our best achievement is that one of our beloved previous GSoC contributor is now the KDE e.V. president!
