---
title: SoK
layout: SoK
menu:
#   main:
#     name: SoK
#     weight: 2
---

Season of KDE is an outreach program hosted by the KDE community.

Every year since 2013, KDE Student Programs has been running Season of KDE as a program similar to, but not quite the same as [Google Summer of Code](../gsoc/index.html), offering an opportunity to everyone (not just students) to participate in both code and non-code projects that benefits the KDE ecosystem. In the past few years, SoK participants have not only contributed new application features but have also developed the KDE Continuous Integration System, statistical reports for developers, a web framework, ported KDE Applications, created documentation and lots and lots of other work.

For some inspiration for your proposals, take a look at our [Ideas](https://community.kde.org/SoK/Ideas/2024) page. 

{{< timeline >}}

    {{% event date="2023-12-15" title="Start of Season of KDE 2024" %}}
    {{% /event %}}

    {{% event date="2024-01-08" title="Deadline for the contributors applications"  invertedText="timeline-inverted" %}}
    {{% /event %}}
    
    {{% event date="2024-01-15" title="Projects announced" %}}
    {{% /event %}}
    
    {{% event date="2024-01-17" title="Start of work" invertedText="timeline-inverted" %}}
    {{% /event %}}
    
    {{% event date="2024-03-31" title="End of work" %}}
    {{% /event %}}
    
    {{% event date="2024-04-07" title="Results announced" invertedText="timeline-inverted" %}}
    {{% /event %}}
    
    {{% event date="2024-05-20" title="Certificates issued" %}}
    {{% /event %}}
    
    {{% event date="After the end" title="Merchandise and swag sent out by courier" invertedText="timeline-inverted" %}}
    {{% /event %}}
{{< /timeline >}}
